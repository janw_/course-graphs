# Course: Graph Theory & Complex Networks

Created at Vrije Universiteit Amsterdam

## Description

A series of Mathematica notebooks used for analyzing parts of graph theory.
